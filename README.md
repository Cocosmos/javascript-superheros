# JavaScript SuperHéros

## Exercice Javascript pour  le 01/03/2021.

### Contraintes :

1. Créer un tableau d'objets de superhéros (minimum 2 entrées).

2. Ouvrir une boîte de dialogue qui nous demande de choisir un numéro (entre 1 et N selon le nombre entrées de votre tableau).

3. Créer une fonction qui va convertir le chiffre donnée en index ( 1-> 0 | 2 ->1 | etc.).

4. Utiliser ce numéro généré par votre précédente fonction pour récupérer les datas du super héros qui correspond a l'index dans le tableau.

5. Afficher ces informations dans une page HTML.