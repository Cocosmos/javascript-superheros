var tableau = [
   {
    superhero:"Batman", 
    publisher:"DC Comics",
    alter_ego:"Bruce Wayne",
    first_appearance:"Detective Comics #27",
    image:'https://resize.programme-television.ladmedia.fr/r/670,670/img/var/premiere/storage/images/tele-7-jours/news-tv/batman-begins-tf1-les-secrets-du-renouveau-4485482/90193586-1-fre-FR/Batman-Begins-TF1-Les-secrets-du-renouveau.jpg',
},
{
    superhero:"Superman", 
    publisher:"DC Comics",
    alter_ego:"Kal-El",
    first_appearance:"Action Comics #1",
    image:'https://wp.unil.ch/allezsavoir/files/2013/05/superman_gd.jpg'
},
{
    superhero:"Flash", 
    publisher:"DC Comics",
    alter_ego:"Jay Garrick",
    first_appearance:"Flash Comics #1",
    image:"http://www.implications-philosophiques.org/wordpress/wp-content/uploads/2015/12/THE-FLASH_612x380.jpg"
},
{
    superhero:"Green Lantern", 
    publisher:"DC Comics", 
    alter_ego:"Alan Scott",
    first_appearance:"All-American Comics #16",
    image:"https://townsquare.media/site/442/files/2016/06/2224860-2011_green_lantern_movie_1680x1050.jpg?w=980&q=75"
}
];

var a = prompt("Choisissez un numéro : 1 ou 2 ou 3 ou 4");

while ((a<1) || (a>4) || (a !==parseInt(a, 10).toString())){  
    alert("Merci de n'entrer que les numéros : 1, 2, 3 ou 4");
    var a = prompt("Choisissez un numéro : 1 ou 2 ou 3 ou 4");
}

para = document.createElement("H1");
para.innerHTML = tableau[a-1].superhero;              
document.body.appendChild(para);

img = document.createElement("img");
img.src = tableau[a-1].image;              
document.body.appendChild(img);

para = document.createElement("p");
para.innerHTML = tableau[a-1].publisher;              
document.body.appendChild(para);

para = document.createElement("p");
para.innerHTML = tableau[a-1].alter_ego;              
document.body.appendChild(para);

para = document.createElement("p");
para.innerHTML = tableau[a-1].first_appearance;              
document.body.appendChild(para);